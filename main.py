from datetime import datetime, timedelta
import smartcharging
import pandas as pd
import numpy as np
import simulator
import json


# Initial timestamp
now = datetime(year=2022, month=11, day=11, hour=0, minute=0, second=0)
# Last timestamp
end = datetime(year=2022, month=11, day=13, hour=0, minute=0, second=0)
# Time step in minutes
Δt = 1


DERS = json.load(open('DATA/ders.json'))
SYSTEM = json.load(open('DATA/system.json'))

load = simulator.Element('DATA/load-1m.csv')
pv = simulator.Element('DATA/pv-1m.csv')
evas = simulator.EVsystem(DERS, SYSTEM)
evas.buildinfo(now)

# First timestep storage value
timestep = [now]
now = now + timedelta(minutes = Δt)
evas.power['Power'].append(0)
evas.power['ts'].append(now.isoformat())


# First control signals generation
control = {}
for der in evas.list.evcs:
    control.update({der : {'Power': 0,'Enable': True}})
    
for der in evas.list.bess:
    control.update({der : {'Power': 0,'Enable': True}})


# Simulation process loop
while end > now:
    # manage ders operation
    evas.power['Power'].append(0)
    if now.minute in evas.scheduling:
        print('Run EVSC algorithm')
        control = smartcharging.ems(evas, pv.get(now), load.get(now), now)
        
    for evcs in evas.list.evcs:
        signal = control[evcs]['Power']
        enable = control[evcs]['Enable']
        if evas.evcs[evcs]['Status'][-1].upper() == 'CHARGING':
            Ecurrent = evas.evcs[evcs]['Energy'][-1]
            Emax = evas.evcs[evcs]['Emax'][-1]

            if Ecurrent + signal*Δt/60 > Emax:
                power = (Emax - Ecurrent)/Δt
                energy = Emax
            else:
                power = signal
                energy = Ecurrent + power*Δt/60
            status = 'charging' 
            SoC = 0 
            if evas.evcs[evcs]['Emax'][-1] > 0: SoC = 100*energy/Emax
            if SoC >= 100: status = 'available'

        if evas.evcs[evcs]['Status'][-1].upper() == 'AVAILABLE':
            status, SoC, Emax, power, energy = simulator.evcharging.arrive(Δt, now.hour)
        
        evas.update_evcs(evcs, now.isoformat(), power, signal, enable, SoC, energy, status, Emax)          
        evas.power['Power'][-1] += power
         
    # Refresh PV
    for der in evas.list.pv:
        power = pv.get(now)/1000
        evas.update_pv(der, now.isoformat(), power)
        evas.power['Power'][-1] -= power

    for der in evas.list.load:
        power = load.get(now)/1000
        evas.update_load(der, now.isoformat(), power)
        evas.power['Power'][-1] -= power

    for bess in evas.list.bess:
        signal = control[bess]['Power']
        evas.update_bess(bess, now, Δt, signal)
        evas.power['Power'][-1] += signal

    
    print(now)
    timestep.append(now)
    evas.power['ts'].append(now.isoformat())
    now = now + timedelta(minutes = Δt)


for evcs in evas.list.evcs:
    data = evas.evcs[evcs]
    simulator.plot.control(evcs, timestep, data['Power'], data['Signal'])
    simulator.plot.energy(evcs, timestep, data['SoC'])


for bess in evas.list.bess:
    data = evas.bess[bess]
    simulator.plot.control(bess, timestep, data['Power'], data['Signal'])
    simulator.plot.energy(bess, timestep, data['SoC'])


for der in evas.list.pv:
    data = evas.pv[der]
    simulator.plot.control(der, timestep, data['Power'], data['Signal'])

for der in evas.list.load:
    data = evas.load[der]
    simulator.plot.control(der, timestep, data['Power'], data['Signal'])

#Plot EVAS operation
simulator.plot.EVASoperation(timestep, evas.power['Power'], evas.Pmax)


print('End of program')