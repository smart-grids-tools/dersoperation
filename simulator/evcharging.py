import numpy as np

probsoc = [0.05, 0.15, 0.35, 0.65, 0.85, 0.95, 1.00]
SoCvec = [20, 30, 40, 50, 60, 70, 80]
Prob = [
#   00:00  01:00  02:00  03:00  04:00  05:00  06:00  07:00  08:00  09:00  10:00  11:00       
    0.001, 0.010, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.010, 0.019,
#   12:00  13:00  14:00  15:00  16:00  17:00  18:00  19:00  20:00  21:00  22:00  23:00       
    0.020, 0.022, 0.040, 0.070, 0.090, 0.130, 0.125, 0.119, 0.110, 0.095, 0.070, 0.020,
]


def arrive(Δt, hour):
    status = 'available'
    SoC = 0.0
    Emax = 0
    power = 0
    energy = 0
    if np.random.random() < (Prob[hour])*(Δt/60):
        status = 'charging'
        currentprob = np.random.uniform(0,1)
        for p in range(0, 7):
            if currentprob <= probsoc[p]:
                SoC = SoCvec[p]
                break
        Emax = 40
        energy = Emax*SoC/100
    else:
        status = 'available'

    return status, SoC, Emax, power, energy