import matplotlib.pyplot as plt
import numpy as np

def control(name, timestep, power, signal):
    plt.rcParams.update({'font.size': 18})
    plt.figure(figsize=(16, 12))
    plt.plot(timestep, power, linewidth='1.5', color = 'red', label='Power')
    plt.plot(timestep, signal, linewidth='1', color = 'blue', label='signal')
    plt.xticks(rotation=45, ha='right')
    plt.ylabel('Power [kW]')
    plt.xlabel('timestamp')
    plt.legend(loc='upper right')
    plt.savefig(f'Figs/{name}.png')
    plt.close('All')


def operation(name, timestep, power):
    plt.rcParams.update({'font.size': 18})
    plt.figure(figsize=(16, 12))
    plt.plot(timestep, power, linewidth='1.5', color = 'red', label='Power')
    plt.xticks(rotation=45, ha='right')
    plt.ylabel('Power [kW]')
    plt.xlabel('timestamp')
    plt.legend(loc='upper right')
    plt.savefig(f'Figs/{name}.png')
    plt.close('All')

def EVASoperation(timestep, power, Pmax):
    lim = np.ones(len(power))*Pmax
    plt.rcParams.update({'font.size': 18})
    plt.figure(figsize=(16, 12))
    plt.plot(timestep, power, linewidth='1.5', color = 'red', label='Power')
    plt.plot(timestep, lim, linewidth='1', color = 'blue', label='Pmax')
    plt.xticks(rotation=45, ha='right')
    plt.ylabel('Power [kW]')
    plt.xlabel('timestamp')
    plt.legend(loc='upper right')
    plt.savefig(f'Figs/EVAS-operation.png')
    plt.close('All')

def energy(name, timestep, SoC):
    plt.rcParams.update({'font.size': 18})
    plt.figure(figsize=(16, 12))
    plt.plot(timestep, SoC, linewidth='1.5', color = 'red', label='SoC')
    plt.xticks(rotation=45, ha='right')
    plt.ylabel('SoC [%]')
    plt.xlabel('timestamp')
    plt.legend(loc='upper right')
    plt.savefig(f'Figs/{name}-SoC.png')
    plt.close('All')