import simulator.evcharging
import simulator.plot
import pandas as pd
import numpy as np


class List:
    def __init__(self):
        self.evcs = list()
        self.bess = list()
        self.pv = list()
        self.load = list()
        return
    
    def all(self):
        return self.evcs + self.bess + self.pv + self.load

class EVsystem:
    def __init__(self, ders, system):
        self.Pmax = system['Pmax']
        self.scheduling = system['EVSC']
        self.ders = ders
        self.evcs = dict()
        self.bess = dict()
        self.pv = dict()
        self.load = dict()
        self.power = {'ts': [], 'Power': []}
    
    def defaultinfo(self, start):
        # Mandatory information for DERs management
        info = {
            'ts':       [start.isoformat()],
            'Power':    [0],
            'Signal':   [0],
            'Enable':   [True],
            'SoC':      [0],
            'Energy':   [0],
            'Status':   ['available'],
            'Emax':     [0]
        }
        return info
    
    def buildinfo(self, start):
        self.system = self.defaultinfo(start)
        self.list = List()
        for der in self.ders:
            if self.ders[der]['Type'].upper() == 'EVCS':
                self.evcs.update({der : self.defaultinfo(start)})
                self.list.evcs.append(der)
            if self.ders[der]['Type'].upper() == 'BESS':
                self.bess.update({der : self.defaultinfo(start)})
                self.bess[der]["SoC"][0] = 50
                self.bess[der]["Energy"][0] = self.ders[der]["Emax"]/2
                self.list.bess.append(der)
            if self.ders[der]['Type'].upper() == 'PV':
                self.pv.update({der: self.defaultinfo(start)})
                self.list.pv.append(der)
            if self.ders[der]['Type'].upper() == 'LOAD':
                self.load.update({der: self.defaultinfo(start)})
                self.list.load.append(der)
        return
    
    def update_evcs(self, name, ts, Power, Signal, Enable, SoC, Energy, Status, Emax):
        self.evcs[name]['ts'].append(ts)
        self.evcs[name]['Power'].append(Power)       
        self.evcs[name]['Signal'].append(Signal)     
        self.evcs[name]['Enable'].append(Enable)     
        self.evcs[name]['SoC'].append(SoC)           
        self.evcs[name]['Energy'].append(Energy)     
        self.evcs[name]['Emax'].append(Emax)         
        self.evcs[name]['Status'].append(Status)

    def update_pv(self, name, ts, Power):
        self.pv[name]['ts'].append(ts)
        self.pv[name]['Power'].append(Power)       
        self.pv[name]['Signal'].append(0)     
        self.pv[name]['Enable'].append(True)     
        self.pv[name]['SoC'].append(0)           
        self.pv[name]['Energy'].append(0)     
        self.pv[name]['Emax'].append(0)         
        self.pv[name]['Status'].append('available')
    
    def update_load(self, name, ts, Power):
        self.load[name]['ts'].append(ts)
        self.load[name]['Power'].append(Power)       
        self.load[name]['Signal'].append(0)     
        self.load[name]['Enable'].append(True)     
        self.load[name]['SoC'].append(0)           
        self.load[name]['Energy'].append(0)     
        self.load[name]['Emax'].append(0)         
        self.load[name]['Status'].append('available')

    def update_bess(self, name, ts, Δt, Signal):
        Ecurrent = self.bess[name]['Energy'][-1]
        if Ecurrent + Signal*Δt/60 > self.ders[name]['Emax']:
            power = (self.ders[name]['Emax'] - Ecurrent)/Δt
            Efinal = self.ders[name]['Emax']
        elif Ecurrent + Signal*Δt/60 < 0:
            power = (0 - Ecurrent)/Δt
            Efinal = 0
        else:
            power = Signal
            Efinal = Ecurrent + power*Δt/60

        self.bess[name]['ts'].append(ts)
        self.bess[name]['Power'].append(power)       
        self.bess[name]['Signal'].append(Signal)     
        self.bess[name]['Enable'].append(True)
        self.bess[name]['Energy'].append(Efinal)     
        self.bess[name]['Emax'].append(self.ders[name]['Emax'])
        self.bess[name]['SoC'].append(100*Efinal/self.ders[name]['Emax'])                  
        self.bess[name]['Status'].append('available')

class Element:
    def __init__(self, path):
        self.hist = pd.read_csv(path, index_col='datetime', parse_dates=True)
        return
    
    def get(self, timestamp):
        value = self.hist.loc[timestamp]
        return value.value