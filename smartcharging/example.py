from datetime import datetime, timedelta
import pandas as pd
import numpy as np
import json

dfpv = pd.read_csv('RESULTS/pv.csv')


for t in range(0,100):
    current = dfpv.iloc[-1]
    nextstep = datetime.fromisoformat(current.time) + timedelta(hours=1) 
    insert = {
        "time": [nextstep.isoformat()], 
        "factor": [np.round(np.random.random(), decimals=2)]
    }
    dfpv = pd.concat([dfpv, pd.DataFrame(insert)])

dfpv.to_csv('RESULTS/newpv.csv')

print('fim do programa')

# if __name__ == "__main__":
#     a = 1