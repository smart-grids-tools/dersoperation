import numpy as np

def ems(evas, pvnow, loadnow, now):
    
    control = {}
    for der in evas.list.evcs:
        control.update({
            der : {
                'Power': np.round(evas.ders[der]['Pmax'] * np.random.random(), decimals=2),
                'Enable': True
            }
        })
    for der in evas.list.bess:
        control.update({
            der : {
                'Power': np.round(evas.ders[der]['Pmax'] * np.random.uniform(-1,1), decimals=2),
                'Enable': True
            }
        })

    return control



if __name__ == "__main__":
    print('Run as main file')